//
//  AppCoreResourceManager.m
//  AppCore
//
//  Created by Thomas Richter on 08.09.11.
//  Copyright 2011 Thomas Richter. All rights reserved.
//

#import "AppCoreResourceManager.h"
#import "SynthesizeSingleton.h"
#import "ASIHTTPRequest.h"
#import "CJSONDeserializer.h"

@implementation AppCoreResourceManager

@synthesize context, resourceNames, resourceDirectory;

SYNTHESIZE_SINGLETON_FOR_CLASS(AppCoreResourceManager);

- (void) loadFiles
{
    if (self.context!=nil) {
        checksums = [[NSMutableDictionary dictionaryWithContentsOfFile:[self.resourceDirectory stringByAppendingPathComponent:@".checksums.plist"]] retain];
        stringResources = [[NSMutableDictionary dictionaryWithContentsOfFile:[self.resourceDirectory stringByAppendingPathComponent:@".strings.plist"]] retain];
        
        if (checksums==nil) checksums = [[NSMutableDictionary alloc] init];
        if (stringResources==nil) stringResources = [[NSMutableDictionary alloc] init];
    }
}
- (void) saveFiles
{
    if (checksums!=nil && self.context!=nil) {
        [checksums writeToFile:[self.resourceDirectory stringByAppendingPathComponent:@".checksums.plist"] atomically:YES];
    }
    if (stringResources!=nil && self.context!=nil) {
        [stringResources writeToFile:[self.resourceDirectory stringByAppendingPathComponent:@".strings.plist"] atomically:YES];
    }
}

- (id)initWithResourceDirectory:(NSString *)directory {
    self = [self init];
    if (self) {
        self.resourceDirectory = directory;
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        resourceDownloadQueue = [[NSOperationQueue alloc] init];
        resourceDownloadQueue.maxConcurrentOperationCount = 1;
    }
    return self;
}

- (NSArray *)resourceNames
{
    NSArray *resources = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:self.resourceDirectory error:nil];
    
    NSMutableArray *names = [NSMutableArray array];
    
    [resources enumerateObjectsUsingBlock:^(id item, NSUInteger idx, BOOL *stop) {
        if (![[item substringToIndex:1] isEqualToString:@"."]) [names addObject:item];
    }];
    
    [stringResources enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL* stop) {
        [names addObject:key]; 
    }];
    
    return names;
}

- (void)ng_runBlockMain:(void (^)())block
{
	block();
}

- (void)saveResourceWithName:(NSString *)name checksum:(NSString *)checksum andUrl:(NSURL *)url
{
    NSString *filename = [self.resourceDirectory stringByAppendingPathComponent:name];
    
    if ([url.scheme isEqualToString:@"bundle"]) {
        NSString *localPathOfBundleFile = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:url.path];
        NSDictionary *attr = [[NSFileManager defaultManager] attributesOfItemAtPath:localPathOfBundleFile error:nil];
        NSDate *lastMod = [attr objectForKey:NSFileModificationDate];
        checksum = [lastMod description];
    }
    
    void (^executeBlocks)(NSString*,NSString*) = ^(NSString *checksum, NSString *name){
        [checksums setObject:checksum forKey:name];
        [self saveFiles];
        NSArray *blockList = [resourceExecutionBlocks objectForKey:name];
        if (blockList!=nil) {
            [blockList enumerateObjectsUsingBlock:^(id block, NSUInteger idx, BOOL *stop) {
                [self performSelectorOnMainThread:@selector(ng_runBlockMain:) withObject:[[block copy] autorelease] waitUntilDone:NO];
            }];
        }                    
    };
    
//    NSLog(@"checksum test for %@: %@==%@?",name, checksum,[checksums objectForKey:name]);
    if ([checksums objectForKey:name]==nil || ![[checksums objectForKey:name] isEqualToString:checksum]) {
        
        if ([url.scheme isEqualToString:@"bundle"]) {
            NSString *path = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:url.path];
            NSError *error = nil;
            if ([[NSFileManager defaultManager] fileExistsAtPath:filename]) {
                [[NSFileManager defaultManager] removeItemAtPath:filename error:nil];
            }
            [[NSFileManager defaultManager] copyItemAtPath:path toPath:filename error:&error];
            if (error!=nil) {
                NSLog(@"failure copying %@ to local resources: %@",url,[error description]);
            }else{
                executeBlocks(checksum,name);
            }
        }else{
        
            //        NSLog(@"downloading %@...",name);
            ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
            request.downloadDestinationPath = filename;
            [request setCompletionBlock:^{
                
                //            NSLog(@"DOWNLOAD OF RESOURCE %@ COMPLETED: %@",name,request.downloadDestinationPath);
                executeBlocks(checksum,name);
                
            }];
            [request setFailedBlock:^{
                //            NSLog(@"DOWNLOAD OF RESOURCE %@ FAILED: %@",name,[request.error description]);
            }];
            [resourceDownloadQueue addOperation:request];
            
        }
    }    
}

- (void) saveStringResourceWithName:(NSString *)name string:(NSString *)theString
{
    [stringResources setObject:theString forKey:name];
    [self saveFiles];
}

-(void)deleteResourceWithName:(NSString *)name
{
    [checksums removeObjectForKey:name];
    [stringResources removeObjectForKey:name];
    [self saveFiles];
    NSString *f = [self pathFromName:name];
    if (f==nil) return;
    [[NSFileManager defaultManager] removeItemAtPath:f error:nil];    
}

- (NSString *)pathFromName:(NSString *)name
{
    NSString *f = [self.resourceDirectory stringByAppendingPathComponent:name];
    if ([[NSFileManager defaultManager] fileExistsAtPath:f]) return f;
    return nil;
}

-(NSURL *)urlFromName:(NSString *)name
{
    NSString *f = [self pathFromName:name];
    if (f==nil) return nil;
    return [NSURL fileURLWithPath:f];
}

- (NSData *)dataFromName:(NSString *)name
{
    NSString *f = [self pathFromName:name];
    if (f==nil) return nil;
    return [NSData dataWithContentsOfFile:f];
}

- (NSArray *)plistArrayFromName:(NSString *)name
{
    NSString *f = [self pathFromName:name];
    if (f==nil) return nil;
    return [NSArray arrayWithContentsOfFile:f];
}

- (NSDictionary *)plistDictionaryFromName:(NSString *)name
{
    NSString *f = [self pathFromName:name];
    if (f==nil) return nil;
    return [NSDictionary dictionaryWithContentsOfFile:f];
}

- (id)decodeJsonFromName:(NSString *)name
{
    return [[CJSONDeserializer deserializer] deserialize:[self dataFromName:name] error:nil];
}

- (NSString *)contentFromName:(NSString *)name withEncoding:(NSStringEncoding)encoding
{
    NSString *f = [self pathFromName:name];
    if (f==nil) return nil;
    return [NSString stringWithContentsOfFile:f encoding:encoding error:nil];
}

- (UIImage *)imageFromName:(NSString *)name
{
    NSString *f = [self pathFromName:name];
    if (f==nil) return nil;
    return [UIImage imageWithContentsOfFile:f];
}

- (NSString *)stringValueFromName:(NSString *)name
{
    return [stringResources objectForKey:name];
}

- (void)dealloc {
    [resourceDownloadQueue release];
    [self removeObserver:self forKeyPath:@"context"];
    [resourceExecutionBlocks release];
    [super dealloc];
}

- (void)resourceAvailableWithName:(NSString *)name withBlock:(void (^)(void))block
{
    if ([self pathFromName:name]!=nil) {
        block();
        block = nil;
    }else{
        if (resourceExecutionBlocks==nil) {
            resourceExecutionBlocks = [[NSMutableDictionary alloc] initWithCapacity:1];
        }
        NSMutableArray *blockList = [resourceExecutionBlocks objectForKey:name];
        if (blockList==nil) {
            blockList = [[NSMutableArray alloc] initWithCapacity:1];
            [resourceExecutionBlocks setObject:blockList forKey:name];
            [blockList release];
        }
        
        [blockList addObject:[[block copy] autorelease]];
        // search for operation and increase priority
        
        NSString *fullFilename = [self.resourceDirectory stringByAppendingPathComponent:name];
        [[resourceDownloadQueue operations] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([[obj class] isSubclassOfClass:[ASIHTTPRequest class]]) {
                if ([((ASIHTTPRequest*)obj).downloadDestinationPath isEqualToString:fullFilename]) {
                    *stop = YES;
                    if (!((ASIHTTPRequest*)obj).isExecuting && !((ASIHTTPRequest*)obj).isCancelled && !((ASIHTTPRequest*)obj).isFinished) {
                        ((ASIHTTPRequest*)obj).queuePriority = NSOperationQueuePriorityVeryHigh;
                        //[AppCore log:[NSString stringWithFormat:@"increased download priority for %@",name]];
                    }
                }
            }
        }];
    }
}

@end
