//
//  AppCoreResourceManager.h
//  AppCore
//
//  Created by Thomas Richter on 08.09.11.
//  Copyright 2011 Thomas Richter. All rights reserved.
//

#import <Foundation/Foundation.h>

#define RM [AppCoreResourceManager sharedAppCoreResourceManager]

@class AppCoreContext;

@interface AppCoreResourceManager : NSObject {
    NSMutableDictionary *checksums;
    NSMutableDictionary *stringResources;
    NSMutableDictionary *resourceExecutionBlocks;
    NSOperationQueue *resourceDownloadQueue;
}

@property (nonatomic, retain) AppCoreContext *context;
@property (nonatomic, readonly) NSArray *resourceNames;
@property (nonatomic, retain) NSString *resourceDirectory;

- (id)initWithResourceDirectory:(NSString *)directory;
- (id)init;

- (void) saveResourceWithName:(NSString *)name checksum:(NSString *)checksum andUrl:(NSURL *)url;
- (void) deleteResourceWithName:(NSString *)name;

- (void) saveStringResourceWithName:(NSString *)name string:(NSString *)theString;

- (NSString *) pathFromName:(NSString *)name;
- (NSURL *) urlFromName:(NSString *)name;

- (NSString *) stringValueFromName:(NSString *)name;

- (UIImage *) imageFromName:(NSString *)name;
- (NSString *)contentFromName:(NSString *)name withEncoding:(NSStringEncoding)encoding;
- (NSData *) dataFromName:(NSString *)name;
- (id) decodeJsonFromName:(NSString *)name;
- (NSArray *) plistArrayFromName:(NSString *)name;
- (NSDictionary *) plistDictionaryFromName:(NSString *)name;

- (void) resourceAvailableWithName:(NSString *)name withBlock:(void (^)(void))block;

+ (AppCoreResourceManager *)sharedAppCoreResourceManager;

@end
